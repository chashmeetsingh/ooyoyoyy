class ApiController < ApplicationController
	after_action :access_control_headers

 def set_access_control_headers
 end

	def all
		render json: Form.all.select("longtitude","latitude","id","created_at")
	end

	def add
		form = Form.create
		form.name=params[:name]
		form.age=params[:age]
		form.gender=params[:gender]
		form.blood_group=params[:blood_group]
		form.latitude=params[:latitude]
		form.longtitude=params[:longtitude]
		#form.profession=params[:profession]
		#form.social_status=params[:social_status]
		if form.save
			render json:"Done"
		else
			render json: "Error"
		end
	end
	def today
		render json: Form.last(4)
	end

	def month

	end
end
