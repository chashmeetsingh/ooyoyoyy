Rails.application.routes.draw do
  root to: "home#index"
  get '/all' => "api#all"
  get '/today'=>"api#today"
  get '/month' => "api#month"
  get '/add/:name/:age/:gender/:blood_group/:latitude/:longtitude' => "api#add", :constraints => { :latitude => /[^\/]+/, :longtitude=> /[^\/]+/ }
end
