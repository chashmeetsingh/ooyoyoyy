class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.string :name
      t.integer :age
      t.string :gender
      t.string :blood_group
      t.decimal :latitude
      t.decimal :longtitude
      t.string :profession
      t.string :social_status
      t.timestamps null: false
    end
  end
end
